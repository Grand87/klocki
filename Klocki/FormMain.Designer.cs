﻿namespace koniks
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMoveBlocks = new System.Windows.Forms.Timer(this.components);
            this.timerDropRandomBlock = new System.Windows.Forms.Timer(this.components);
            this.panelMain = new System.Windows.Forms.Panel();
            this.labelLivesValue = new System.Windows.Forms.Label();
            this.labelLives = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelTimeValue = new System.Windows.Forms.Label();
            this.labelPoints = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonLevel1 = new System.Windows.Forms.Button();
            this.ButtonLevel2 = new System.Windows.Forms.Button();
            this.ButtonLevel3 = new System.Windows.Forms.Button();
            this.timerPause = new System.Windows.Forms.Timer(this.components);
            this.labelPointsValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timerMoveBlocks
            // 
            this.timerMoveBlocks.Interval = 1;
            this.timerMoveBlocks.Tick += new System.EventHandler(this.timerMoveBlock_Tick);
            // 
            // timerDropRandomBlock
            // 
            this.timerDropRandomBlock.Interval = 500;
            this.timerDropRandomBlock.Tick += new System.EventHandler(this.timerDropRandomBlock_Tick);
            // 
            // panelMain
            // 
            this.panelMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMain.Location = new System.Drawing.Point(6, 5);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(425, 349);
            this.panelMain.TabIndex = 1;
            // 
            // labelLivesValue
            // 
            this.labelLivesValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLivesValue.AutoSize = true;
            this.labelLivesValue.ForeColor = System.Drawing.Color.Red;
            this.labelLivesValue.Location = new System.Drawing.Point(505, 116);
            this.labelLivesValue.Name = "labelLivesValue";
            this.labelLivesValue.Size = new System.Drawing.Size(13, 13);
            this.labelLivesValue.TabIndex = 6;
            this.labelLivesValue.Text = "5";
            // 
            // labelLives
            // 
            this.labelLives.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLives.AutoSize = true;
            this.labelLives.Location = new System.Drawing.Point(437, 116);
            this.labelLives.Name = "labelLives";
            this.labelLives.Size = new System.Drawing.Size(36, 13);
            this.labelLives.TabIndex = 5;
            this.labelLives.Text = "Życia:";
            // 
            // labelTime
            // 
            this.labelTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(437, 90);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(33, 13);
            this.labelTime.TabIndex = 4;
            this.labelTime.Text = "Czas:";
            // 
            // labelTimeValue
            // 
            this.labelTimeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTimeValue.AutoSize = true;
            this.labelTimeValue.Location = new System.Drawing.Point(505, 90);
            this.labelTimeValue.Name = "labelTimeValue";
            this.labelTimeValue.Size = new System.Drawing.Size(13, 13);
            this.labelTimeValue.TabIndex = 3;
            this.labelTimeValue.Text = "0";
            // 
            // labelPoints
            // 
            this.labelPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPoints.AutoSize = true;
            this.labelPoints.Location = new System.Drawing.Point(437, 142);
            this.labelPoints.Name = "labelPoints";
            this.labelPoints.Size = new System.Drawing.Size(63, 13);
            this.labelPoints.TabIndex = 2;
            this.labelPoints.Text = "Trafionych: ";
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.Location = new System.Drawing.Point(517, 12);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(55, 47);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(434, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Level:";
            // 
            // ButtonLevel1
            // 
            this.ButtonLevel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonLevel1.Location = new System.Drawing.Point(437, 28);
            this.ButtonLevel1.Name = "ButtonLevel1";
            this.ButtonLevel1.Size = new System.Drawing.Size(20, 23);
            this.ButtonLevel1.TabIndex = 8;
            this.ButtonLevel1.Text = "1";
            this.ButtonLevel1.UseVisualStyleBackColor = true;
            this.ButtonLevel1.Click += new System.EventHandler(this.ButtonLevel1_Click);
            // 
            // ButtonLevel2
            // 
            this.ButtonLevel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonLevel2.Location = new System.Drawing.Point(462, 28);
            this.ButtonLevel2.Name = "ButtonLevel2";
            this.ButtonLevel2.Size = new System.Drawing.Size(20, 23);
            this.ButtonLevel2.TabIndex = 8;
            this.ButtonLevel2.Text = "2";
            this.ButtonLevel2.UseVisualStyleBackColor = true;
            this.ButtonLevel2.Click += new System.EventHandler(this.buttonLevel2_Click);
            // 
            // ButtonLevel3
            // 
            this.ButtonLevel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonLevel3.Location = new System.Drawing.Point(487, 28);
            this.ButtonLevel3.Name = "ButtonLevel3";
            this.ButtonLevel3.Size = new System.Drawing.Size(20, 23);
            this.ButtonLevel3.TabIndex = 8;
            this.ButtonLevel3.Text = "3";
            this.ButtonLevel3.UseVisualStyleBackColor = true;
            this.ButtonLevel3.Click += new System.EventHandler(this.ButtonLevel3_Click);
            // 
            // timerPause
            // 
            this.timerPause.Interval = 500;
            this.timerPause.Tick += new System.EventHandler(this.timerPause_Tick);
            // 
            // labelPointsValue
            // 
            this.labelPointsValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPointsValue.AutoSize = true;
            this.labelPointsValue.Location = new System.Drawing.Point(505, 142);
            this.labelPointsValue.Name = "labelPointsValue";
            this.labelPointsValue.Size = new System.Drawing.Size(13, 13);
            this.labelPointsValue.TabIndex = 2;
            this.labelPointsValue.Text = "0";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 366);
            this.Controls.Add(this.ButtonLevel3);
            this.Controls.Add(this.ButtonLevel2);
            this.Controls.Add(this.ButtonLevel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelLivesValue);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.labelLives);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelPointsValue);
            this.Controls.Add(this.labelPoints);
            this.Controls.Add(this.labelTimeValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gra zręcznościowa - klocki";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMoveBlocks;
        private System.Windows.Forms.Timer timerDropRandomBlock;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelPoints;
        private System.Windows.Forms.Label labelTimeValue;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelLives;
        private System.Windows.Forms.Label labelLivesValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonLevel1;
        private System.Windows.Forms.Button ButtonLevel2;
        private System.Windows.Forms.Button ButtonLevel3;
        private System.Windows.Forms.Timer timerPause;
        private System.Windows.Forms.Label labelPointsValue;
    }
}

