﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace koniks
{

    public partial class FormMain : Form
    {
        //wielkość bloku w pixelach
        const int blockSize = 20;
        const int startLifes = 5;
        const int chence = 20;

        FallingBlock[] blocks;          //główna tablica blaków
        int blockCount;                 //licznik bloków na planszy
        int lifesCounter;               //licznik żyć        
        int pointsCounter;              //licznik punktów
        int blinkCounter;               //licznik mrugnięć po kliknięciu zielonego
        DateTime startTime;             //czas rozpoczęcia gry
        Random random = new Random();   //zmienna losująca liczby

        public FormMain()
        {
            InitializeComponent();
        }

        //funkcja rozpocznająca nową grę
        private void StartGame()
        {
            //wyczyszczenie wszystkich danych
            ClearAll();

            //utworzenie bloków
            GenerateBlocks();

            //wyłączenie przycisków na czas gry (false)
            EnableButtons(false);

            timerMoveBlocks.Start(); ;
            timerDropRandomBlock.Start();
        }

        //funkcja włączająca/wyłączająca przyciski
        private void EnableButtons(bool enable)
        {
            //przycisk startu
            buttonStart.Enabled = enable;
            //przyciski poziomów trudności
            ButtonLevel1.Enabled = enable;
            ButtonLevel2.Enabled = enable;
            ButtonLevel3.Enabled = enable;
        }

        //funkcja czyszcząca stan gry
        private void ClearAll()
        {
            //przywrócenie żyć
            lifesCounter = startLifes;
            labelLivesValue.Text = lifesCounter.ToString();

            //wyzerowanie punktów
            pointsCounter = 0;
            labelPointsValue.Text = pointsCounter.ToString();


            //ustawienie czasu rozpoczęcia gry
            startTime = DateTime.Now;

            //czyszczenie wszysstkich widocznych klocków
            panelMain.Controls.Clear();
        }

        //funkcja generująca bloki
        private void GenerateBlocks()
        {
            //policzenie ile bloków zmieści się w pionie
            blockCount = panelMain.Width / (blockSize + 5);

            //utworzenie tablicy o wyliczonej wielkości
            blocks = new FallingBlock[blockCount];

            //utworzenie wyglądu każdego z bloku
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i] = new FallingBlock()
                {
                    Parent = panelMain, //ustawienie rodzica (jest to panel po którym będą spadać)
                    Width = blockSize, //szerokość bloku
                    Height = blockSize, //wysokość bloku
                    Top = -blockSize, //początkowa pozycja bloku na panelu (-20 oznacza że będzie ponad nim czyli niewidoczny)
                    Left = blockSize * i + 5, //ustawienie pozycji od lewej. Każdy znajduje się obok siebie 
                    isFalling = false, //czy blok spada? na początku nie
                    TabStop = false, //uniemożliwienie używania przycisku Tab do przechodzenie po blokach
                };

                //podpięcie funkcji która będzie wywoływać się po naciśnięciu na blok
                blocks[i].MouseClick += ButtonMouseClick;
            }
        }

        //funkcja wywoływana na końcu gry
        private void EndGame()
        {
            //zatrzymanie timerów
            timerMoveBlocks.Stop();
            timerDropRandomBlock.Stop();

            EnableButtons(true);

            //wyświetlenie podsumowania
            MessageBox.Show(labelPoints.Text + "\n W czasie: " + labelTimeValue.Text);
        }

        //funkcja przenosi wszystkie bloku na górę
        private void SendAllBlocksToTop()
        {
            foreach (var block in blocks)
            {
                SendBlockToTop(block);
            }
        }

        //funkcja przenosi wybrany blok na górę
        private void SendBlockToTop(FallingBlock fallingBlock)
        {
            fallingBlock.isBomb = false;
            fallingBlock.isStoper = false;
            fallingBlock.BackColor = buttonStart.BackColor;
            fallingBlock.Top = -blockSize;
            fallingBlock.isFalling = false;
        }

        //funkcja timera przesuwająca bloki w dół
        private void timerMoveBlock_Tick(object sender, EventArgs e)
        {
            foreach (var block in blocks)
            {
                //jeśli blok spada to
                if (block.isFalling)
                {
                    //przesunięcie bloku w dół o jeden pixel
                    block.Top += 1;

                    //sprawdzenie czy blok spadł już na sam dół
                    CheckIfBlockIsOnBottom(block);
                }
            }

            //zaktualizowanie czasu który upłynął
            labelTimeValue.Text = (DateTime.Now - startTime).ToString();
        }

        //funkcja sprawdza czy blok spadł na sam dół
        private void CheckIfBlockIsOnBottom(FallingBlock block)
        {
            //jeśli blok spadł czyli jego pozycja jest po za wysokością panelu
            if (panelMain.Height < block.Top)
            {
                //przeniesienie bloku na górę
                SendBlockToTop(block);

                //odjęcie życia
                lifesCounter--;

                //zaktualizowanie wyświetlanej wartości życia
                labelLivesValue.Text = lifesCounter.ToString();

                //jeśli skończyły się życia - koniec gry
                if (lifesCounter <= 0)
                {
                    EndGame();
                }
            }
        }

        //funkcja rozpoczynająca spadanie losowego bloku
        private void timerDropRandomBlock_Tick(object sender, EventArgs e)
        {
            //wylosowanie numeru bloku
            int numer = random.Next(0, blockCount);
            //uruchomienie spadania bloku
            blocks[numer].isFalling = true;

            //wylosowanie ewentualnej bomby lub stopera
            RandomBombOrStoper();
        }

        //funkcja przycisku uruchamiającego grę
        private void buttonStart_Click(object sender, EventArgs e)
        {
            StartGame();
        }

        //funkcja uruchamiana po kliknięciu myszką na blok
        private void ButtonMouseClick(object sender, MouseEventArgs e)
        {
            //pobranie który blok został kliknięty
            var block = (sender as FallingBlock);

            //zwiększenie licznika punktów
            pointsCounter++;

            //zaktualizowanie wyświetlanej wartości punktów
            labelPointsValue.Text = pointsCounter.ToString();

            //jeśli kliknięto bombę
            if (block.isBomb)
            {
                //przeniesienie wszystkich bloków na górę
                SendAllBlocksToTop();
            }
            //jeśli klinięto stoper
            else if (block.isStoper)
            {
                //zapałzuj bloki
                PauseBlocks();
            }

            //przeniesienie kliknietego bloku na górę
            SendBlockToTop(block);
        }

        //funkcja pauzująca bloki
        private void PauseBlocks()
        {
            //ilość mrugnięć (jednocześnie długość pauzy)
            blinkCounter = 10;
            //zatrzymanie bloków
            timerMoveBlocks.Stop();
            timerDropRandomBlock.Stop();
            //uruchomienie timera pauzy
            timerPause.Start();
        }

        //funkcja losująca bombę lub stoper
        private void RandomBombOrStoper()
        {
            //pobranie z listy tych bloków, które nie spadają
            var stoppedBlock = blocks.Where(b => !b.isFalling).ToList();
            //jeśli wszystkie są w ruchu - wyjście z funkcji
            if (stoppedBlock.Count == 0)
            {
                return;
            }

            //wylosowanie numeru bloku
            var randomBlockNumber = random.Next(0, stoppedBlock.Count() - 1);
            //pobranie bloku o wylosowanym numerze
            var randomBlock = stoppedBlock[randomBlockNumber];

            //wylosowanie czy blok ma być bombą czy stoperem (szansa 1/20)
            //-wylosowana liczba to 3 -> bomba
            //-wylosowana liczba to 5 -> stoper
            var randomValue = random.Next(0, chence);
            if (randomValue == 3)
            {
                randomBlock.isStoper = true;
                //zmiana koloru tła na zielony
                randomBlock.BackColor = Color.Green;
            }
            else if (randomValue == 5)
            {
                randomBlock.isBomb = true;
                //zmiana koloru tła na czerwony
                randomBlock.BackColor = Color.Red;
            }
        }

        //funkcje zmieniające poziomy trudności (czyli szerokość planszy)
        private void ButtonLevel1_Click(object sender, EventArgs e)
        {
            this.Width = 600;
        }

        private void buttonLevel2_Click(object sender, EventArgs e)
        {
            this.Width = 800;
        }

        private void ButtonLevel3_Click(object sender, EventArgs e)
        {
            this.Width = 1000;
        }

        //funkcja wywoływana przez timer pauzy
        private void timerPause_Tick(object sender, EventArgs e)
        {
            //zmiejszenie licznika mrugnięć
            blinkCounter--;

            //dla każdego z bloku ustawienie odpowidniego tła w zależności od stanu:
            foreach (var block in blocks)
            {
                //jeśli licznik mrugnięć się wyczerpał - koniec pauzy
                if (blinkCounter == 0)
                {
                    //zmiana mrugającego tła na szare (jeśli to nie bomba lub stoper)
                    if (!block.isBomb && !block.isStoper)
                        block.BackColor = buttonStart.BackColor;

                    //zatrzymnie timera pauzy
                    timerPause.Stop();

                    //uruchominie spadania bloków
                    timerMoveBlocks.Start();
                    timerDropRandomBlock.Start();

                }
                //jeśli licznik pauzy się nie wyszerpał
                else if (!block.isBomb && !block.isStoper)
                {
                    //zmiana koloru tła na przeciwny. Jeśli był szary - na biały i odwrotnie
                    if (block.BackColor == buttonStart.BackColor)
                    {
                        block.BackColor = Color.White;
                    }
                    else
                    {
                        block.BackColor = buttonStart.BackColor;
                    }
                }
            }

        }
    }

    //klasa reprezentująca spadający blok - zawiera informację czy jest w ruchu/czy jest bombą/czy stoperem
    class FallingBlock : Button
    {
        public bool isFalling;
        public bool isBomb;
        public bool isStoper;
    }
}
